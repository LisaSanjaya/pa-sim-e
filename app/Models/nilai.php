<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nilai extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'id_ekstrakurikuler',
        'id_users',
        'nomor_induk',
        'nama',
        'kelas',
        'nilai',
        'deskripsi'
    ];

    protected $guarded = [];
}
