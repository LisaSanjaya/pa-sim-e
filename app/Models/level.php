<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class level extends Model
{
    protected $table = 'levels';
    protected $primaryKey = "id_levels";
    protected $fillable = ['id_role', 'nama_levels'];

    public function user()
    {
        return $this->hasMany(User::class);
    }
}
