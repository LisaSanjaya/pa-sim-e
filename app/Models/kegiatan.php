<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kegiatan extends Model
{
    use HasFactory;
    protected $table = 'kegiatan';
    protected $fillable = ['waktu', 'keterangan', 'id_ekstrakurikuler', 'ekstrakurikuler'];

    public function ekstrakurikuler()
    {
        return $this->belongsTo(Ekstrakurikuler::class);
    }

    public function anggotas()
    {
        return $this->belongsTo(Anggota::class);
    }
}
