<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ekstrakurikuler extends Model
{
    use HasFactory;
    protected $table = 'ekstrakurikuler';
    protected $guarded = [];

    public function anggotas()
    {
        return $this->belongsTo(Anggota::class);
    }

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class);
    }

    public function prestasi()
    {
        return $this->belongsTo(Prestasi::class);
    }
}
