<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mengikuti extends Model
{
    use HasFactory;

    protected $table = 'mengikuti';
    protected $primaryKey = 'id_mengikuti';

    public $timestamps = false;

    protected $fillable = [
        'ekstrakurikuler_id',
        'anggota_id',
    ];

    public function anggotas()
    {
        return $this->belongsTo(Anggota::class, 'anggota_id', 'id', );
    }

    public function ekstrakurikuler()
    {
        return $this->belongsTo(Ekstrakurikuler::class, 'ekstrakurikuler_id', 'id');
    }
}