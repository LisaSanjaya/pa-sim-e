<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'nomor_induk',
        'email',
        'password',
        'level',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the anggota associated with the User.
     */
    public function isAdmin()
    {
        // Ganti dengan kondisi yang sesuai untuk menentukan apakah user adalah pembina Pramuka
        return $this->id === 2; // Contoh ID pembina Pramuka adalah 2
    }

    public function isPembinaPramuka()
    {
        // Ganti dengan kondisi yang sesuai untuk menentukan apakah user adalah pembina Pramuka
        return $this->id === 11; // Contoh ID pembina Pramuka adalah 2
    }

    public function isPembinaTataBoga()
    {
        // Ganti dengan kondisi yang sesuai untuk menentukan apakah user adalah pembina Tata Boga
        return $this->id === 6; // Contoh ID pembina Tata Boga adalah 3
    }

    public function ekstrakurikuler()
    {
        return $this->belongsToMany(Ekstrakurikuler::class, 'anggota', 'id_users');
    }

    public function anggota()
    {
        return $this->hasMany(anggota::class);
    }
}
