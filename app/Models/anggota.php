<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nama',
        'nomor_induk',
        'kelas',
        'tapel',
        'semester',
        'ekstrakurikuler_id',
        'id_users',
    ];

    public function ekstrakurikuler()
    {
        return $this->belongsTo(Ekstrakurikuler::class);
    }

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class);
    }

    public function prestasi()
    {
        return $this->belongsTo(Prestasi::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id_users','id');
    }


    
}
