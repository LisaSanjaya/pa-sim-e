<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class prestasi extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_users',
        'nomor_induk',
        'nama',
        'id_ekstrakurikuler',
        'kelas',
        'prestasi',
    ];
    protected $guarded = [];

    public function ekstrakurikuler()
    {
        return $this->belongsTo(Ekstrakurikuler::class);
    }

    public function anggotas()
    {
        return $this->belongsTo(Anggota::class);
    }
}
