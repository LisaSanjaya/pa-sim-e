<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class absensi_tataboga extends Model
{
    protected $table = 'absensi_tataboga';
    protected $fillable = ['user_id', 'tanggal'];
    public $timestamps = false;


    // Relasi dengan model Pembina
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
