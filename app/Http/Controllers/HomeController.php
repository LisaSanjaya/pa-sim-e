<?php

namespace App\Http\Controllers;

use App\Models\anggota;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $totalTataBoga = anggota::where('ekstrakurikuler', 'Tata Boga')->count();
        $Totalekstra = anggota::count();

        return view('beranda', compact('anggota'));
    }
}
