<?php

namespace App\Http\Controllers;

use App\Models\ekstrakurikuler;
use App\Models\Anggota;
use App\Models\nilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NilaiController extends Controller
{
    public function tataboga()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }
    public function pramuka()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }
    public function tpq()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }
    public function pencak()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga',compact('nilai','ekstrakurikuler'));
    }
    public function banjari()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga',compact('nilai','ekstrakurikuler'));
    }
    public function olahraga()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }
    public function komputer()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }
    public function bisnis()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $nilai = nilai::all();
        return view('nilai.nilaitataboga', compact('nilai','ekstrakurikuler'));
    }

    public function index()
    {
        // Cek peran pengguna saat ini
        $nilai = nilai::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        $id_user = Auth::user()->id;

    if ($id_user == '2') {
        // Jika peran adalah admin, tampilkan semua data kegiatan
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
    } else {
        // Cari anggota berdasarkan ID pengguna
        $anggota = Anggota::where('id_users', $id_user)->first();

        if ($anggota) {
            $ekstrakurikuler = $anggota->ekstrakurikuler;
            $nilai = Nilai::where('id_ekstrakurikuler', $ekstrakurikuler->id)->get();
        } else {
            // Tidak ada anggota dengan ID pengguna yang diberikan
            $nilai = collect();
            $ekstrakurikuler = collect();
        }
    }
    
        return view('nilai.nilai', compact('nilai','ekstrakurikuler'));
    }

    public function index_admin()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('nilai.nilai_admin', compact(['ekstrakurikuler']));
    }

    public function show($ekstrakurikulerId)
    {
        $nilaiAnggota = Nilai::where('ekstrakurikuler_id', $ekstrakurikulerId)->get();

        return view('nilai.nilai_umum', compact('nilaiAnggota'));
    }

    public function create()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('nilai.createnilai',compact('ekstrakurikuler', 'anggota'));
    }


    public function store(Request $request)
    {
        
        nilai::create($request->except(['_token', 'submit']));
        return redirect('/nilai');
    }

    public function destroy($id)
    {
        $nilai = nilai::find($id);
        $nilai->delete();
        return redirect('nilai');
    }

    public function edit($id)
    {
        $nilai = nilai::find($id);
        return view('nilai.editnilai', compact(['nilai']));
    }

    public function update($id, Request $request)
    {
        $nilai = nilai::find($id);
        $nilai->update($request->except(['_token', 'submit']));
        return redirect('nilai');
    }
}
