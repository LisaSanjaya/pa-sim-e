<?php

namespace App\Http\Controllers;

use App\Models\prestasi;
use App\Models\Anggota;
use App\Models\ekstrakurikuler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class PrestasiController extends Controller
{
    public function admin()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasiadmin', compact('prestasi', 'ekstrakurikuler'));
    }
    public function tataboga()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasitataboga', compact('prestasi', 'ekstrakurikuler'));
    }
    public function tpq()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasitpq', compact('prestasi', 'ekstrakurikuler'));
    }
    public function pencak()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasipencak', compact('prestasi', 'ekstrakurikuler'));
    }
    public function pramuka()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasipramuka', compact('prestasi', 'ekstrakurikuler'));
    }
    public function bisnis()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasibisnis', compact('prestasi', 'ekstrakurikuler'));
    }
    public function banjari()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasibanjari', compact('prestasi', 'ekstrakurikuler'));
    }
    public function komputer()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasikomputer', compact('prestasi', 'ekstrakurikuler'));
    }
    public function olahraga()
    {
        $prestasi = prestasi::all();
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('prestasi.prestasiolahraga', compact('prestasi', 'ekstrakurikuler'));
    }

    public function index()
{
    // Cek peran pengguna saat ini
    $id_user = Auth::user()->id;

    if ($id_user == '2') {
        // Jika peran adalah admin, tampilkan semua data kegiatan
        $prestasi = Prestasi::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
    } else {
        // Cari anggota berdasarkan ID pengguna
        $anggota = Anggota::where('id_users', $id_user)->first();

        if ($anggota) {
            $ekstrakurikuler = $anggota->ekstrakurikuler;
            $prestasi = prestasi::where('id_ekstrakurikuler', $ekstrakurikuler->id)->get();
        } else {
            // Tidak ada anggota dengan ID pengguna yang diberikan
            $kegiatan = collect();
            $ekstrakurikuler = collect();
        }
    }
    return view('prestasi.prestasi', compact('prestasi', 'ekstrakurikuler'));
}

    public function create()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('prestasi.createprestasi', compact( 'ekstrakurikuler'));
    }

    public function store(Request $request)
    {

        $id_user = Auth::user()->id;
        $data = $request->except(['_token', 'submit']);
        $data['id_users'] = $id_user;
    
        prestasi::create($data);
        return redirect('prestasi');
    }

    public function destroy($id)
    {
        $prestasi = prestasi::find($id)->first();
        File::delete(public_path('foto') . '/' . $prestasi->foto);
        $prestasi = prestasi::find($id);
        $prestasi->delete();
        return redirect('prestasi');
    }
}
