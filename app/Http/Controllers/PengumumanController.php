<?php

namespace App\Http\Controllers;

use App\Models\ekstrakurikuler;
use App\Models\pengumuman;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    public function index()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $pengumuman = pengumuman::all();
        return view('pengumuman.pengumuman', compact(['pengumuman', 'ekstrakurikuler']));
    }

    public function mengedit()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $pengumuman = pengumuman::all();
        return view('pengumuman.pgm', compact(['pengumuman', 'ekstrakurikuler']));
    }

    public function create()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('pengumuman.createpengumuman',compact('ekstrakurikuler'));
    }

    public function store(Request $request)
    {
        pengumuman::create($request->except(['_token', 'submit']));
        return redirect('pengumuman');
    }

    public function destroy($id)
    {
        $pengumuman = pengumuman::find($id);
        $pengumuman->delete();
        return redirect('/pengumuman');
    }

    public function edit($id)
    {
        $pengumuman = pengumuman::find($id);
        return view('pengumuman.editpengumuman', compact(['pengumuman']));
    }

    public function update($id, Request $request)
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $pengumuman = pengumuman::find($id);
        $pengumuman->update($request->except(['_token', 'submit']));
        return redirect('/pengumuman');
    }
}
