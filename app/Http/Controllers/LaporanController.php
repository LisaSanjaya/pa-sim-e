<?php

namespace App\Http\Controllers;

use App\Models\ekstrakurikuler;
use Illuminate\Http\Request;
use App\Models\anggota;
use App\Models\kegiatan;
use App\Models\nilai;
use App\Models\prestasi;
use PDF;

class LaporanController extends Controller
{
    public function index()
    {
        $list = ekstrakurikuler::all();
        return view('laporan.laporan', compact(['list']));
    }

    public function tataboga()
    {
        return view('laporan.cetaktataboga');
    }
    public function anggotaboga()
    {
        $anggota = Anggota::all(); 
        return view('laporan.pdfanggotatataboga', compact('anggota'));
    }
    public function prestasiboga()
    {
        $prestasi = prestasi::all(); 
        return view('laporan.pdfprestasitataboga', compact('prestasi'));
    }
    public function kegiatanboga()
    {
        $kegiatan = kegiatan::all(); 
        return view('laporan.pdfkegiatantataboga', compact('kegiatan'));
    }
    public function nilaiboga()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $prestasi = prestasi::all(); 
        $anggota = Anggota::all(); 
        $nilai = nilai::all(); 
        return view('laporan.pdfnilaiboga', compact('nilai', 'ekstrakurikuler', 'prestasi', 'anggota'));
    }


    public function komputer()
    {
        return view('laporan.cetakkomputer');
    }
    public function tpq()
    {
        return view('laporan.cetaktpq');
    }
    public function olahraga()
    {
        return view('laporan.cetakolahraga');
    }
    public function bisnis()
    {
        return view('laporan.cetakbisnis');
    }
    public function pencak()
    {
        return view('laporan.cetakpencak');
    }
    public function banjari()
    {
        return view('laporan.cetakbanjari');
    }
    public function pramuka()
    {
        return view('laporan.cetakpramuka');
    }
}
