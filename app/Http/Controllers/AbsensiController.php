<?php

namespace App\Http\Controllers;

use Absensi;
use App\Models\absensi_tataboga;
use App\Models\absensi_siswa;
use App\Models\presensi;
use App\Models\anggota;
use App\Models\ekstrakurikuler;
use App\Models\history_presensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AbsensiController extends Controller
{
    public function siswa()
    {
    $id_user = Auth::user()->id;
    $absensi = history_presensi::where('anggota_id', $id_user)->get();
    $anggota = Anggota::where('id_users', $id_user)->first();
    $ekstrakurikuler = Ekstrakurikuler::find($anggota->ekstrakurikuler_id);
    
    // $absensi = absensi_tataboga::all();
        
    return view('absensi.absensi_siswa',compact('anggota','ekstrakurikuler','absensi'));
    }

    public function pembina()
{
    // $id_user = Auth::user()->id;
    $absensi = presensi::where('users_id', Auth::user()->id)->get();
    // $anggota = Anggota::where('id_users', $id_user)->first();
    // $ekstrakurikuler = Ekstrakurikuler::find($anggota->ekstrakurikuler_id);

    return view('absensi.absensi_pembina', compact( 'absensi'));
}

public function absenPembina(Request $request)
{
    
    $ekstrakurikuler_id = ekstrakurikuler::where('id_users', auth()->user()->id)->first()->id;
    // dd($ekstrakurikuler_id);
    $absensiPembina = new presensi();
    $absensiPembina->ekstrakurikuler_id = $ekstrakurikuler_id;
    
    $absensiPembina->status = 'buka';
    $absensiPembina->tanggal = now(); // Menggunakan tanggal dan waktu saat ini
    $absensiPembina->users_id = Auth::user()->id;
    $absensiPembina->save();
    
    // Mengubah nilai absensiDibuka menjadi true
    // $request->session()->put('absensiDibuka', true);

    return redirect()->back();
}
public function absenSiswa(Request $request)
{
//    dd(auth()->user()->id);
    $id_user = Auth::user()->id;
    $anggota = Anggota::where('id_users', $id_user)->first();
    // $ekstrakurikuler_id = ekstrakurikuler::where('id_users', auth()->user()->id)->first();
    // dd($ekstrakurikuler_id);
    $absensiSiswa = new history_presensi();
    $absensiSiswa->ekstrakurikuler_id = $anggota->ekstrakurikuler_id;
    $absensiSiswa->anggota_id = $id_user;
    $absensiSiswa->presensi_id = $request->presensi_id;
    $absensiSiswa->tanggal = now(); // Menggunakan tanggal dan waktu saat ini
    $absensiSiswa->save();

    return redirect()->back();
}
        // public function destroy($id)
        // {
        //     $absensi = absensi_tataboga::find($id);
        //     $absensi->delete();
        //     return redirect('/absen-tataboga');
        // }
    public function tutup_presensi($id)
    {
        $presensi = presensi::find($id);
        $presensi->status = 'tutup';
        $presensi->save();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $absensi = presensi::find($id);
        $absensi->delete();
        return redirect()->back();
    }
    
}
