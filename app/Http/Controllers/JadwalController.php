<?php

namespace App\Http\Controllers;

use App\Models\ekstrakurikuler;
use App\Models\jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
    $jadwal = jadwal::all();
    return view('jadwal.jadwal', compact(['ekstrakurikuler', 'jadwal']));
    }
    public function create()
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        return view('jadwal.createjadwal',compact('ekstrakurikuler'));
    }
    public function store(Request $request)
    {
        jadwal::create($request->except(['_token', 'submit']));
        return redirect('/jadwal');
    }
    
    public function destroy($id)
    {
        $jadwal = jadwal::find($id);
        $jadwal->delete();
        return redirect('/jadwal');
    }
    public function edit($id)
    {
        $ekstrakurikuler = ekstrakurikuler::all();
        $jadwal = jadwal::find($id);
        return view('jadwal.editjadwal', compact(['jadwal']));
    }
    public function update($id, Request $request)
    {
        $jadwal = jadwal::find($id);
        $jadwal->update($request->except(['_token', 'submit']));
        return redirect('/jadwal');
    }
}
