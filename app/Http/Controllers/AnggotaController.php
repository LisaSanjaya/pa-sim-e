<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Anggota;
use App\Models\ekstrakurikuler;
use App\Models\Mengikuti;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AnggotaController extends Controller
{
    public function daftar()
    {
        return view('daftar');
    }
    public function admin()
    {
        $anggota = Anggota::all();
        return view('anggota.anggotaadmin', compact(['anggota']));
    }
    public function tataboga()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
         return view('anggota.anggotatataboga', compact('anggota', 'ekstrakurikuler'));
    }
    public function pramuka()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotapramuka', compact('anggota', 'ekstrakurikuler'));
    }
    public function pencak()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotapencak', compact('anggota', 'ekstrakurikuler'));
    }
    public function tpq()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotatpq', compact('anggota', 'ekstrakurikuler'));
    }
    public function komputer()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotakomputer', compact('anggota', 'ekstrakurikuler'));
    }
    public function olahraga()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotaolahraga', compact('anggota', 'ekstrakurikuler'));
    }
    public function bisnis()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotabisnis', compact('anggota', 'ekstrakurikuler'));
    }
    public function banjari()
    {
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.anggotabanjari', compact('anggota', 'ekstrakurikuler'));
    }


    public function index()
{
    // Cek peran pengguna saat ini
    $id_user = Auth::user()->id;

    if ($id_user == '2') {
        // Jika peran adalah admin, tampilkan semua data kegiatan
        $anggota = Anggota::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
    } else {
        // Cari anggota berdasarkan ID pengguna
        $anggota = Anggota::where('id_users', $id_user)->first();

        if ($anggota) {
            $ekstrakurikuler = $anggota->ekstrakurikuler;
            $anggota = Anggota::where('ekstrakurikuler_id', $ekstrakurikuler->id)->get();
        } else {
            // Tidak ada anggota dengan ID pengguna yang diberikan
            $anggota = collect();
            $ekstrakurikuler = collect();
        }
    }

    return view('anggota.anggota', compact('anggota', 'ekstrakurikuler'));
}

    public function create()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('anggota.createanggota', compact('ekstrakurikuler'));
    }

    public function store(Request $request)
{
    $user = User::where('email', '=', $request->email)->first();

    if ($user) {
        $id_user = $user->id;

        // Periksa apakah pengguna sudah terdaftar untuk ekstrakurikuler manapun
        $anggota = Anggota::where('id_users', $id_user)->first();

        if ($anggota) {
            // Jika pengguna sudah terdaftar sebagai anggota ekstrakurikuler
            return redirect()->back()->with('error', 'Anda sudah terdaftar sebagai anggota ekstrakurikuler.');
        }

        // Lanjutkan dengan menyimpan anggota baru
        $data = $request->except(['_token', 'submit']);
        $data['id_users'] = $id_user;
        $anggota = Anggota::create($data);

        return redirect('anggota_ekstrakurikuler');
    } else {
        // Handle jika User tidak ditemukan
        return redirect()->back()->with('error', 'User tidak ditemukan.');
    }
}
    
    public function destroy($id)
    {
        $anggota = Anggota::find($id);
        $anggota->delete();
        return redirect('anggota');
    }

    public function edit($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editanggota', compact('anggota'));
}

    public function update($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota');
    }

    public function edit_boga($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editboga', compact('anggota'));
    }

    public function update_boga($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_tataboga');
    }
    public function destroy_boga($id)
    {
        $anggota = Anggota::find($id);
        $anggota->delete();
        return redirect('anggota_tataboga');
    }
    public function edit_bisnis($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editbisnis', compact('anggota'));
    }

    public function update_bisnis($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_bisnis');
    }
    public function edit_tpq($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.edittpq', compact('anggota'));
    }

    public function update_tpq($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_tpq');
    }
    public function edit_olahraga($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editolahraga', compact('anggota'));
    }

    public function update_olahraga($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_olahraga');
    }
    public function edit_pencak($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editpencak', compact('anggota'));
    }

    public function update_pencak($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_pencak');
    }
    public function edit_banjari($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editbanjari', compact('anggota'));
    }

    public function update_banjari($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_banjari');
    }
    public function edit_komputer($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editkomputer', compact('anggota'));
    }

    public function update_komputer($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_komputer');
    }
    public function edit_pramuka($id)
    {
        $anggota = Anggota::find($id);
    return view('anggota.editpramuka', compact('anggota'));
    }

    public function update_pramuka($id, Request $request)
    {
        $anggota = Anggota::find($id);
        $anggota->update($request->except(['_token', 'submit']));
        return redirect('/anggota_pramuka');
    }
}
