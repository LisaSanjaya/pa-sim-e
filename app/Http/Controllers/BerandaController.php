<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\pengumuman;
use App\Models\ekstrakurikuler;
use App\Models\anggota;
use App\Models\jadwal;

class BerandaController extends Controller
{
    public function index()
    {
        $Totaltataboga = anggota::where('ekstrakurikuler_id', 1)->count();
        $Totalalbanjari = anggota::where('ekstrakurikuler_id', 7)->count();
        $TotalBisnisPlan = anggota::where('ekstrakurikuler_id', 6)->count();
        $TotalOlahraga = anggota::where('ekstrakurikuler_id', 5)->count();
        $TotalPramuka = anggota::where('ekstrakurikuler_id', 3)->count();
        $TotalKomputer = anggota::where('ekstrakurikuler_id', 9)->count();
        $TotalPendidikanTPQ = anggota::where('ekstrakurikuler_id', 8)->count();
        $Totalpencaksilat = anggota::where('ekstrakurikuler_id', 4)->count();
        $ekstrakurikuler = ekstrakurikuler::all();
        $pengumuman = pengumuman::all();
        $jadwal = jadwal::all();
        return view('beranda', compact(
            'Totaltataboga',
            'Totalalbanjari',
            'TotalBisnisPlan',
            'TotalOlahraga',
            'TotalPramuka',
            'TotalKomputer',
            'TotalPendidikanTPQ',
            'Totalpencaksilat',
            'pengumuman',
            'jadwal','ekstrakurikuler'
        ));
    }
}
