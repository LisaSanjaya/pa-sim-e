<?php

namespace App\Http\Controllers;

use App\Models\kegiatan;
use App\Models\ekstrakurikuler;
use Illuminate\Http\Request;
use App\Models\Anggota;
use App\Models\history_presensi;
use Faker\Provider\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class KegiatanController extends Controller
{
    
    public function admin()
    {
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanekstra', compact(['kegiatan']));
    }
    public function banjari()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanbanjari', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function pramuka()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanpramuka', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function tataboga()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $TotalAbsensi = history_presensi::where('ekstrakurikuler_id', 1)->count();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatantataboga',  compact('kegiatan', 'ekstrakurikuler'));
    }
    public function tpq()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatantpq', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function olahraga()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanolahraga', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function komputer()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatankomputer', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function bisnis()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanbisnis', compact('kegiatan', 'ekstrakurikuler'));
    }
    public function pencak()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        $kegiatan = kegiatan::all();
        return view('kegiatan.kegiatanpencak', compact('kegiatan', 'ekstrakurikuler'));
    }

    public function index()
{
    // Cek peran pengguna saat ini
    $id_user = Auth::user()->id;

    if ($id_user == '2') {
        // Jika peran adalah admin, tampilkan semua data kegiatan
        $kegiatan = Kegiatan::all();
        $ekstrakurikuler = Ekstrakurikuler::all();
    } else {
        // Cari anggota berdasarkan ID pengguna
        $anggota = Anggota::where('id_users', $id_user)->first();

        if ($anggota) {
            $ekstrakurikuler = $anggota->ekstrakurikuler;
            $kegiatan = Kegiatan::where('id_ekstrakurikuler', $ekstrakurikuler->id)->get();
        } else {
            // Tidak ada anggota dengan ID pengguna yang diberikan
            $kegiatan = collect();
            $ekstrakurikuler = collect();
        }
    }

    return view('kegiatan.kegiatan', compact('kegiatan', 'ekstrakurikuler'));
}

    public function create()
    {
        $ekstrakurikuler = Ekstrakurikuler::all();
        return view('kegiatan.createkegiatan',compact('ekstrakurikuler'));
    }

    public function store(Request $request)
    {
        kegiatan::create($request->except(['_token', 'submit']));
        return redirect('kegiatan');
    }

    public function destroy($id)
    {
        $kegiatan = kegiatan::find($id)->first();
        $kegiatan = kegiatan::find($id);
        $kegiatan->delete();
        return redirect('kegiatan');
    }

    public function edit($id)
    {
        $kegiatan = kegiatan::find($id);
        return view('kegiatan.editkegiatan', compact(['kegiatan']));
    }

    public function update($id, Request $request)
    {
        $kegiatan = kegiatan::find($id);
        $kegiatan->update($request->except(['_token', 'submit']));
        return redirect('/kegiatan');
    }
}
