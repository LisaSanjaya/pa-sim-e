<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\user;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {

        $data = [
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'nama' => $request->nama,
            'kelas' => $request->kelas,
            'level' => $request->level
        ];
        user::create($data);
        return redirect('/login');
    }
}
