<?php

namespace App\Http\Controllers;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\PrestasiController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\AbsensiController;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing.dash');
});

//beranda
Route::get('home', [BerandaController::class, 'index']);

//login
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/postlogin', [LoginController::class, 'postlogin']);
Route::get('/logout', [LoginController::class, 'logout']);

//register
Route::get('/register', [RegisterController::class, 'register'])->name('register');
Route::post('register/store', [RegisterController::class, 'store']);


Route::middleware(['auth'])->group(function () {
    //CRUD Nilai
    Route::get('nilai', [NilaiController::class, 'index']);
    Route::get('list_ekstrakurikuler', [NilaiController::class, 'index_admin']);
    Route::get('nilai/create', [NilaiController::class, 'create']);
    Route::get('nilai/create/tataboga', [NilaiController::class, 'createtataboga']);
    Route::post('store', [NilaiController::class, 'store']);
    Route::delete('nilai/{id}', [NilaiController::class, 'destroy']);
    Route::get('nilai/{id}/edit', [NilaiController::class, 'edit']);
    Route::put('nilai/{id}', [NilaiController::class, 'update']);
    Route::get('nilai_tataboga', [NilaiController::class, 'tataboga']);
    Route::get('nilai_pramuka', [NilaiController::class, 'pramuka']);
    Route::get('nilai_tpq', [NilaiController::class, 'tpq']);
    Route::get('nilai_komputer', [NilaiController::class, 'komputer']);
    Route::get('nilai_olahraga', [NilaiController::class, 'olahraga']);
    Route::get('nilai_banjari', [NilaiController::class, 'banjari']);
    Route::get('nilai_bisnis', [NilaiController::class, 'binis']);
    Route::get('nilai_pencak', [NilaiController::class, 'pencak']);
   
    //CRUD Pengumuman
    Route::get('pengumuman', [PengumumanController::class, 'index']);
    Route::get('pengumuman/create', [PengumumanController::class, 'create']);
    Route::get('pengumuman/edit', [PengumumanController::class, 'mengedit']);
    Route::post('pengumuman/store', [PengumumanController::class, 'store']);
    Route::delete('pengumuman/{id}', [PengumumanController::class, 'destroy']);
    Route::get('pengumuman/{id}/edit', [PengumumanController::class, 'edit']);
    Route::put('pengumuman/{id}', [PengumumanController::class, 'update']);


    //CRUD kegiatan
    Route::get('kegiatan', [KegiatanController::class, 'index']);
    Route::get('kegiatan_ekstrakurikuler', [KegiatanController::class, 'admin']);
    Route::get('kegiatan_pramuka', [KegiatanController::class, 'pramuka']);
    Route::get('kegiatan_tpq', [KegiatanController::class, 'tpq']);
    Route::get('kegiatan_pencak', [KegiatanController::class, 'pencak']);
    Route::get('kegiatan_bisnis', [KegiatanController::class, 'bisnis']);
    Route::get('kegiatan_olahraga', [KegiatanController::class, 'olahraga']);
    Route::get('kegiatan_komputer', [KegiatanController::class, 'komputer']);
    Route::get('kegiatan_banjari', [KegiatanController::class, 'banjari']);
    Route::get('kegiatan_tataboga', [KegiatanController::class, 'tataboga']);
    Route::get('kegiatan/create', [KegiatanController::class, 'create']);
    Route::post('kegiatan/store', [KegiatanController::class, 'store']);
    Route::delete('kegiatan/{id}', [KegiatanController::class, 'destroy']);
    Route::get('kegiatan/{id}/edit', [KegiatanController::class, 'edit']);
    Route::put('kegiatan/{id}', [KegiatanController::class, 'update']);

    //CRUD jadwal
    Route::get('jadwal', [JadwalController::class, 'index']);
    Route::get('jadwal/create', [JadwalController::class, 'create']);
    Route::post('jadwal/store', [JadwalController::class, 'store']);
    Route::delete('jadwal/{id}', [JadwalController::class, 'destroy']);
    Route::get('jadwal/{id}/edit', [JadwalController::class, 'edit']);
    Route::put('jadwal/{id}', [JadwalController::class, 'update']);


    //CRUD anggota
    Route::get('anggota', [AnggotaController::class, 'index']);
    Route::get('anggota_ekstrakurikuler', [AnggotaController::class, 'admin']);
    Route::get('anggota_tataboga', [AnggotaController::class, 'tataboga']);
    Route::get('anggota_pramuka', [AnggotaController::class, 'pramuka']);
    Route::get('anggota_tpq', [AnggotaController::class, 'tpq']);
    Route::get('anggota_pencak', [AnggotaController::class, 'pencak']);
    Route::get('anggota_bisnis', [AnggotaController::class, 'bisnis']);
    Route::get('anggota_banjari', [AnggotaController::class, 'banjari']);
    Route::get('anggota_olahraga', [AnggotaController::class, 'olahraga']);
    Route::get('anggota_komputer', [AnggotaController::class, 'komputer']);
    Route::get('anggota/create', [AnggotaController::class, 'create']);
    Route::post('anggota/store', [AnggotaController::class, 'store']);
    Route::delete('anggota/{id}', [AnggotaController::class, 'destroy']);
    Route::get('anggota/{id}/edit', [AnggotaController::class, 'edit']);
    Route::put('anggota/{id}', [AnggotaController::class, 'update']);
    Route::get('tataboga/{id}/edit', [AnggotaController::class, 'edit_boga']);
    Route::put('tataboga/{id}', [AnggotaController::class, 'update_boga']);
    Route::delete('tataboga/{id}', [AnggotaController::class, 'destroy_boga']);
    Route::get('pramuka/{id}/edit', [AnggotaController::class, 'edit_pramuka']);
    Route::put('pramuka/{id}', [AnggotaController::class, 'update_pramuka']);
    Route::get('banjari/{id}/edit', [AnggotaController::class, 'edit_banjari']);
    Route::put('banjari/{id}', [AnggotaController::class, 'update_banjari']);
    Route::get('bisnis/{id}/edit', [AnggotaController::class, 'edit_bisnis']);
    Route::put('bisnis/{id}', [AnggotaController::class, 'update_bisnis']);
    Route::get('tpq/{id}/edit', [AnggotaController::class, 'edit_tpq']);
    Route::put('tpq/{id}', [AnggotaController::class, 'update_tpq']);
    Route::get('komputer/{id}/edit', [AnggotaController::class, 'edit_komputer']);
    Route::put('komputer/{id}', [AnggotaController::class, 'update_komputer']);
    Route::get('olahraga/{id}/edit', [AnggotaController::class, 'edit_olahraga']);
    Route::put('olahraga/{id}', [AnggotaController::class, 'update_olahraga']);
    Route::get('pencak/{id}/edit', [AnggotaController::class, 'edit_pencak']);
    Route::put('pencak/{id}', [AnggotaController::class, 'update_pencak']);

    //CRUD prestasi
    Route::get('prestasi', [PrestasiController::class, 'index']);
    Route::get('prestasi_ekstrakurikuler', [PrestasiController::class, 'admin']);
    Route::get('prestasi_bisnis', [PrestasiController::class, 'bisnis']);
    Route::get('prestasi_olahraga', [PrestasiController::class, 'olahraga']);
    Route::get('prestasi_tpq', [PrestasiController::class, 'tpq']);
    Route::get('prestasi_tataboga', [PrestasiController::class, 'tataboga']);
    Route::get('prestasi_pramuka', [PrestasiController::class, 'pramuka']);
    Route::get('prestasi_komputer', [PrestasiController::class, 'komputer']);
    Route::get('prestasi_banjari', [PrestasiController::class, 'banjari']);
    Route::get('prestasi_pencak', [PrestasiController::class, 'pencak']);
    Route::get('prestasi/create', [PrestasiController::class, 'create']);
    Route::post('prestasi/store', [PrestasiController::class, 'store']);
    Route::delete('prestasi/{id}', [PrestasiController::class, 'destroy']);

    //Laporan
    Route::get('laporan', [LaporanController::class, 'index']);
    Route::get('cetak_tataboga', [LaporanController::class, 'tataboga']);
    Route::get('anggota-tataboga', [LaporanController::class, 'anggotaboga']);
    Route::get('prestasi-tataboga', [LaporanController::class, 'prestasiboga']);
    Route::get('nilai-tataboga', [LaporanController::class, 'nilaiboga']);
    Route::get('kegiatan-tataboga', [LaporanController::class, 'kegiatanboga']);
    Route::get('cetak_pramuka', [LaporanController::class, 'pramuka']);
    Route::get('cetak_tpq', [LaporanController::class, 'tpq']);
    Route::get('cetak_olahraga', [LaporanController::class, 'olahraga']);
    Route::get('cetak_bisnis', [LaporanController::class, 'bisnis']);
    Route::get('cetak_komputer', [LaporanController::class, 'komputer']);
    Route::get('cetak_banjari', [LaporanController::class, 'banjari']);
    Route::get('cetak_pencak', [LaporanController::class, 'pencak']);
    Route::get('cetak-anggota-tataboga', [LaporanController::class, 'pencak']);


    //absensi
    Route::get('absensi_tataboga', [AbsensiController::class, 'tataboga']);
    Route::get('absensi_pramuka', [AbsensiController::class, 'pramuka']);
    Route::get('absensi_tpq', [AbsensiController::class, 'tpq']);
    Route::get('absensi_olahraga', [AbsensiController::class, 'olahraga']);
    Route::get('absensi_bisnis', [AbsensiController::class, 'bisnis']);
    Route::get('absensi_komputer', [AbsensiController::class, 'komputer']);
    Route::get('absensi_banjari', [AbsensiController::class, 'banjari']);
    Route::get('absensi_pencak', [AbsensiController::class, 'pencak']);
    Route::get('absensi_siswa', [AbsensiController::class, 'siswa']);
    Route::get('absensi_pembina', [AbsensiController::class, 'pembina']);
    Route::post('absen-pembina', [AbsensiController::class, 'absenPembina'])->name('absen.pembina');
    Route::post('absen-siswa', [AbsensiController::class, 'absenSiswa'])->name('absen.siswa');
    Route::get('tutup_presensi/{id}',[AbsensiController::class, 'tutup_presensi']);
    // Route::delete('/absen-tataboga/{id}', [AbsensiController::class, 'destroy'])->name('absen.destroy');




    Route::get('/daftar', function () {
        return view('daftar');
    });

    Route::get('/coba', function () {
        return view('laporan.coba');
    });

    Route::get('/absensi', function () {
        return view('absensi_');
    });
});


//Details
Route::get('/pramuka-details', function () {
    return view('landing.pramuka-details');
});
Route::get('/boga-details', function () {
    return view('landing.boga-details');
});
Route::get('/business-details', function () {
    return view('landing.business-details');
});
Route::get('/komputer-details', function () {
    return view('landing.komputer-details');
});
Route::get('/olahraga-details', function () {
    return view('landing.olahraga-details');
});
Route::get('/tpq-details', function () {
    return view('landing.tpq-details');
});
Route::get('/banjari-details', function () {
    return view('landing.banjari-details');
});
