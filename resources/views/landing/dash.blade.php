@extends('landing.aps')

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  @section('title', 'SIM EKSKUL')
  @section('content')
    <!-- Main content -->
 
      <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
      <h1 class="mb-4 pb-0">Selamat Datang di<br> <span>Sistem Informasi Manajemen</span> Ekstrakurikuler</h1>
      <p class="mb-4 pb-0">Madrasah Aliyah Yasmu Manyar Gresik</p>
    </div>
  </section><!-- End Hero Section -->

  <main id="main">

    <!-- ======= Speakers Section ======= -->
    <section id="speakers">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h2>Ekstrakurikuler</h2>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="100">
              <img src="assets/img/speakers/PRAMUKA.jpeg" alt="Speaker 1" class="img-fluid">
              <div class="details">
                <h3><a href="pramuka-details">Pramuka</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="200">
              <img src="assets/img/speakers/KOMPUTER.png" alt="Speaker 2" class="img-fluid">
              <div class="details">
                <h3><a href="komputer-details">Komputer</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="300">
              <img src="assets/img/speakers/TATA BOGA.jpeg" alt="Speaker 3" class="img-fluid">
              <div class="details">
                <h3><a href="business-details">Business Plan</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="100">
              <img src="assets/img/speakers/TPQ.jpeg" alt="Speaker 4" class="img-fluid">
              <div class="details">
                <h3><a href="tpq-details">Pendidikan TPQ dan Tahfidz</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="200">
              <img src="assets/img/speakers/FUTSAL.jpeg" alt="Speaker 5" class="img-fluid">
              <div class="details">
                <h3><a href="olahraga-details">Olahraga Prestasi</a></h3>
              </div>
            </div>
          </div>
      {{--   <div class="col-lg-4 col-md-6">
            <div class="speaker" data-aos="fade-up" data-aos-delay="300">
              <img src="assets/img/speakers/TB.jpeg" alt="Speaker 6" class="img-fluid">
              <div class="details">
                <h3><a href="boga-details">Tata Boga</a></h3>
              </div>
            </div>
          </div>
        </div>--}}
        <div class="col-lg-4 col-md-6">
          <div class="speaker" data-aos="fade-up" data-aos-delay="300">
            <img src="assets/img/speakers/BANJARI.jpeg" alt="Speaker 6" class="img-fluid">
            <div class="details">
              <h3><a href="banjari-details">AL-Banjari</a></h3>
            </div>
      </div>

    </section><!-- End Speakers Section -->

    <!-- ======= Venue Section ======= -->
    <section id="venue">

      <div class="container-fluid" data-aos="fade-up">


        <div class="row g-0">

      </div>

      <div class="container-fluid venue-gallery-container" data-aos="fade-up" data-aos-delay="100">
        <div class="row g-0">

        </div>
      </div>

    </section><!-- End Venue Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="section-bg">

      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2>Contact Us</h2>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="bi bi-geo-alt"></i>
              <h3>Alamat</h3>
              <address>JL. Kyai Sahlan | No.24 Manyarejo, Kec. Manyar Kab. Gresik, Jawa Timur Indoensia</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="bi bi-phone"></i>
              <h3>Nomor Telepon</h3>
              <p><a href="tel:+155895548855">031 3930037</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="bi bi-envelope"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@example.com">mayasmu@gmail.com</a></p>
            </div>
          </div>

        </div>
      </div>
    </section><!-- End Contact Section -->
 @endsection

   <!-- Vendor JS Files -->
   <script src="assets/vendor/aos/aos.js"></script>
   <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
   <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
   <script src="assets/vendor/php-email-form/validate.js"></script>
 
   <!-- Template Main JS File -->
   <script src="assets/js/main.js"></script>
