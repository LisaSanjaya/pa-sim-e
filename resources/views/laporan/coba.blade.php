<div class="invoice">
    <div class="" style="position: relative;">
        <div>
            <table width="90%" border="0">
                <tbody>
                    <tr>
                      <td width="266" rowspan="6"><img src="{{ asset('NiceAdmin') }}/assets/img/Logo Nganjuk monocrome.jpg" width="75" alt=""/></td>
                      <td width="1100" align="center"><strong>PEMERINTAH DAERAH KABUPATEN NGANJUK</strong></td>
                    </tr>
                    <tr>
                        <td align="center"><strong>DINAS LINGKUNGAN HIDUP</strong></td>
                    </tr>
                    <tr>
                        <td align="center">Jalan Raya Kedondong Nomor 01 Nganjuk Kode Pos 64461</td>
                    </tr>
                    <tr>
                        <td align="center">Telepon (0358) 328380 / Fax (0358) 328475</td>
                    </tr>
                </tbody>
            </table>
            <hr size="5">
        </div>
        <div>
            <table width="90%" border="0">
                <tbody>
                    <tr>
                        <td width="800"></td>
                        <td colspan="3">Lembar Ke : 1</td>
                    </tr>
                    <tr>
                        <td width="800"></td>
                        <td colspan="3">Kode No : 2</td>
                    </tr>
                    <tr>
                        <td width="800"></td>
                        <td colspan="3">Nomor :3</td>
                    </tr>
                </tbody>
            </table><br>
        </div>
        <div>
            <table class="table table-bordered" width="100%">
              <tbody>

                <tr>
                  <td width="16"> 1.</td>
                  <td width="350">Pejabat yang memberi perintah </td>
                  <td colspan="2">Kepala Dinas Lingkungan Hidup Kabupaten Nganjuk</td>
                  </tr>
                <tr>
                    <td>2. </td>
                    <td> a. Nama Pegawai yang diperintah<br>
                      b. NIP<br></td>
                    <td colspan="2"> a. lisa<br>
                      b.  lisa<br></td>
                    </tr>
                </tr>
                <tr>
                    <td>3. </td>
                    <td> a. Pangkat dan Golongan<br>
                      b. Jabatan<br>
                      c. Tingkat menurut peraturan perjalanan <br></td>
                    <td colspan="2"> a. lisabr>
                      b. lisa<br></td>
                    </tr>
                </tr>
                <tr>
                  <td>4. </td>
                  <td>Maksud Perjalanan </td>
                  <td colspan="2"> lisa</td>
                </tr>
                <tr>
                  <td>5. </td>
                  <td>Alat angkut yang dipergunakan </td>
                  <td colspan="2"> lisa</td>
                </tr>
                <tr>
                  <td>6. </td>
                  <td> a. Tempat berangkat<br>
                    b. Tempat tujuan<br></td>
                  <td colspan="2"> a.  lisa<br>
                    b.  lisa<br></td>
                  </tr>
                <tr>
                  <td>7. </td>
                  <td>
                      a. Lamanya perjalanan dinas <br>
                      b. Tanggal Keberangkatan <br>
                      c. Tanggal harus kembali
                    </td>
                  <td colspan="2">a.  lisa hari<br>
                    b.  lisa<br>
                    c.  lisa<br></td>
                </tr>

                <tr>
                  <td>8. </td>
                  <td width="400">Pengikut</td>
                  <td>-</td>
                </tr>
                {{--  @if ($pengikut)
                  @foreach ($pengikut as $row) <?php $no++  ?>
                  <tr>
                      <td>&nbsp;</td>
                      <td>{{ $no }}. {{ $row[0] }}</td>
                      <td>{{ $row[1] }}</td>
                      <td>{{ $row[2] }}</td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                @endif  --}}
                <tr>
                  <td>9. </td>
                  <td>Pembebanan<br>
                    a. Instansi<br>
                    b. Mata Anggaran<br></td>
                  <td colspan="2"> <br> a. Dinas Lingkungan Hidup Kabupaten Nganjuk<br>
                    b.<br></td>
                  </tr>
                <tr>
                  <td>10. </td>
                  <td>Keterangan Lain-lain</td>
                  <td colspan="2"> lisa</td>
                  </tr>
              </tbody>
            </table>
        </div>
        <div>
            <table width="100%" height="276" border="0">
              <tbody>
                <tr>
                  <td width="319" height="18">&nbsp;</td>
                  <td width="74">&nbsp;</td>
                  <td width="143">Dikeluarkan Di </td>
                  <td width="158">: Cirebon </td>
                </tr>
                <tr>
                  <td height="18">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Pada Tanggal </td>
                  <td>: dhjewb</td>
                </tr>
                <tr>
                  <td height="18" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                  <td height="18">&nbsp;</td>
                  <td colspan="3" align="center">Nganjuk, lisa<br>
                    KEPALA DINAS LINGKUNGAN HIDUP <br>
                    KABUPATEN NGANJUK</td>
                </tr>
                <tr>
                  <td height="76" colspan="4">&nbsp;</td>
                </tr>
                <tr>
                  <td height="18">&nbsp;</td>
                  <td colspan="3" align="center"><u>SUBANI, SH., MM. </u></td>
                </tr>
                <tr>
                  <td height="18">&nbsp;</td>
                  <td colspan="3" align="center">Pembina Utama Muda</td>
                </tr>
                <tr>
                  <td height="18">&nbsp;</td>
                  <td colspan="3" align="center">NIP. 196910051989031007</td>
                </tr>
              </tbody>
            </table><br><br>
        </div>
    </div>
</div>

<style>
    .invoice {
      font-family: 'Times New Roman', Times, serif;
      font-size: 100%;
    }
  </style>