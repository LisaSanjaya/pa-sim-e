<div class="invoice">
    <div class="" style="position: relative;">
        <div>
            <table width="90%" border="0">
                <tbody>
                    <tr>
                        <td width="266" rowspan="6" align="center"><img src="/images/logo.jpeg" width="100" alt=""/></td>
                        <td width="1100" align="center"><strong>YAYASAN MU'ALLIMIN MU'ALIMAT</strong></td>
                    </tr>
                    <tr>
                        <td align="center"><strong>MADRASAH ALIYAH YASMU</strong></td>
                    </tr>
                    <tr>
                        <td align="center"><strong>MANYAR GRESIK</strong></td>
                    </tr>
                    <tr>
                        <td align="center">Jl. Ky. Sahlan I/24 Manyarejo Telp. 031 3930037</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered" width="100%">
              <tbody>

                <tr>
                  <td width="350">Nama : Lisa Hidayatus Sholihah</td>
                  <td colspan="2">Madrasah : MA Yasmu</td>
                  </tr>
                <tr> 
                    <td> NISN : <br>
                      Kelas : <br></td>
                    <td colspan="2">Semester :<br>
                      Tahun Pelajaran : <br></td>
                    </tr>
                </tr>
                 <hr size="5">
              </tbody>
            </table>
            <hr size="5">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">A. Ekstrakurikuler </h3>
                </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="width: 100%; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="width: 10px; border: 1px solid #000;">No</th>
                            <th style="border: 1px solid #000;">Ekstrakurikuler</th>
                            <th style="border: 1px solid #000;">Predikat</th>
                            <th style="border: 1px solid #000;">Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $nomorUrut = 1;
                        @endphp
                        @foreach ($nilai as $agt)
                            @if ($agt->nama == 'Lisa Hidayatus Sholihah')
                                <tr>
                                    <td style="width: 10px; border: 1px solid #000;">{{ $nomorUrut }}</td>
                                    <td style="border: 1px solid #000;">{{ $ekstrakurikuler->where('id', $agt->id_ekstrakurikuler)->first()->nama }}</td>
                                    <td style="border: 1px solid #000;">{{ $agt->nilai }}</td>
                                    <td style="border: 1px solid #000;">{{ $agt->deskripsi }}</td>
                                </tr>
                                @php
                                    $nomorUrut++;
                                @endphp
                            @endif
                        @endforeach
                    </tbody>
                </table>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">B. Prestasi</h3>
                    </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped" style="width: 100%; border-collapse: collapse;">
                        <thead>
                            <tr>
                                <th style="width: 10px; border: 1px solid #000;">No</th>
                                <th style="border: 1px solid #000;">Ekstrakurikuler</th>
                                <th style="border: 1px solid #000;">Prestasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $nomorUrut = 1;
                            @endphp
                            @foreach ($prestasi as $agt)
                                @if ($agt->nama == 'Lisa Hidayatus Sholihah')
                                    <tr>
                                        <td style="width: 10px; border: 1px solid #000;">{{ $nomorUrut }}</td>
                                        <td style="border: 1px solid #000;">{{ $ekstrakurikuler->where('id', $agt->id_ekstrakurikuler)->first()->nama }}</td>
                                        <td style="border: 1px solid #000;">{{ $agt->prestasi }}</td>
                                    </tr>
                                    @php
                                        $nomorUrut++;
                                    @endphp
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">C. Kehadiran</h3>
                        </div>
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped" style="width: 100%; border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <th style="width: 10px; border: 1px solid #000;">No</th>
                                    <th style="border: 1px solid #000;">Ekstrakurikuler</th>
                                    <th style="border: 1px solid #000;">Prestasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $nomorUrut = 1;
                                @endphp
                                @foreach ($prestasi as $agt)
                                    @if ($agt->nama == 'Lisa Hidayatus Sholihah')
                                        <tr>
                                            <td style="width: 10px; border: 1px solid #000;">{{ $nomorUrut }}</td>
                                            <td style="border: 1px solid #000;">{{ $ekstrakurikuler->where('id', $agt->id_ekstrakurikuler)->first()->nama }}</td>
                                            <td style="border: 1px solid #000;">{{ $agt->prestasi }}</td>
                                        </tr>
                                        @php
                                            $nomorUrut++;
                                        @endphp
                                    @endif
                                @endforeach
                            </tbody>
                        </table> --}}
                        <script type="text/javascript">
                            window.print();
                        </script>
            </div>
        </div>
    </div>
</div>

<style>
    .invoice {
      font-family: 'Times New Roman', Times, serif;
      font-size: 100%;
    }
  </style>