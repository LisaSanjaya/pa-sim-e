@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  @section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cetak Data Ekstrakurikuler Komputer</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Keterangan</th>
                    <th>Cetak</th>                 
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Anggota</td>
                    <td>
                        <a href="#" class="btn btn-success"><i class="fas fa-print"></i> excel </a>
                        <a href="#" class="btn btn-primary"><i class="fas fa-print"></i> word </a>
                        <a href="#" class="btn btn-danger"><i class="fas fa-print"></i>pdf </a>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Prestasi</td>
                    <td>
                        <a href="#" class="btn btn-success"><i class="fas fa-print"></i> excel </a>
                        <a href="#" class="btn btn-primary"><i class="fas fa-print"></i> word </a>
                        <a href="#" class="btn btn-danger"><i class="fas fa-print"></i>pdf </a>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Kegiatan</td>
                    <td>
                        <a href="#" class="btn btn-success"><i class="fas fa-print"></i> excel </a>
                        <a href="#" class="btn btn-primary"><i class="fas fa-print"></i> word </a>
                        <a href="#" class="btn btn-danger"><i class="fas fa-print"></i>pdf </a>
                    </td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Nilai</td>
                    <td>
                        <a href="#" class="btn btn-success"><i class="fas fa-print"></i> excel </a>
                        <a href="#" class="btn btn-primary"><i class="fas fa-print"></i> word </a>
                        <a href="#" class="btn btn-danger"><i class="fas fa-print"></i>pdf </a>
                    </td>
                  </tr>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
 @endsection

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- Page specific script -->
</body>
</html>
