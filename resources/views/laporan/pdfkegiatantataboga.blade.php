<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <br>
                <table width="90%" border="0">
                    <tbody>
                        <tr>
                            <td width="266" rowspan="6" align="center"><img src="/images/logo.jpeg" width="100" alt=""/></td>
                            <td width="1100" align="center"><strong>YAYASAN MU'ALLIMIN MU'ALIMAT</strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>MADRASAH ALIYAH YASMU</strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>MANYAR GRESIK</strong></td>
                        </tr>
                        <tr>
                            <td align="center">Jl. Ky. Sahlan I/24 Manyarejo Telp. 031 3930037</td>
                        </tr>
                    </tbody>
                </table>
                <hr size="5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Kegiatan Ekstrakurikuler Tata Boga</h3>
                    </div>
                    
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped" style="width: 100%; border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <th style="width: 10px; border: 1px solid #000;">No</th>
                                    <th style="border: 1px solid #000;">Waktu</th>
                                    <th style="border: 1px solid #000;">Keterangan</th>
                                    <th style="border: 1px solid #000;">Foto</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $nomorUrut = 1;
                                @endphp
                                @foreach ($kegiatan as $agt)
                                    @if ($agt->id_ekstrakurikuler == '1')
                                        <tr>
                                            <td style="width: 10px; border: 1px solid #000;">{{ $nomorUrut }}</td>
                                            <td style="border: 1px solid #000;">{{ $agt->waktu}}</td>
                                            <td style="border: 1px solid #000;">{{ $agt->keterangan }}</td>
                                            <td style="border: 1px solid #000;">{{ $agt->dokumentasi }}</td>
                                        </tr>
                                        @php
                                            $nomorUrut++;
                                        @endphp
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <script type="text/javascript">
                        window.print();
                    </script>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->