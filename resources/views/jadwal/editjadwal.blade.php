@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  @section('title', 'ANGGOTA EKSTRAKULIKULER | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

              <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Edit Jadwal Ekstrakurikuler</h3>
              </div>
              <!-- /.card-header -->
              <form action="/jadwal/{{$jadwal->id}}" method="POST">
                @method('put')
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputwaktu">waktu</label>
                    <input type="text" class="form-control" name="waktu" placeholder="waktu" value="{{$jadwal->waktu}}">
                  </div>
                  <div class="form-group">
                    <label>Ekstrakurikuler</label>
                    <select class="form-control" name="id_ekstrakurikuler">
                      <option value= "1" @if($jadwal->id_ekstrakurikuler == "1") selected @endif >Tata Boga</option>
                      <option value= "3" @if($jadwal->id_ekstrakurikuler == "3") selected @endif>Pramuka</option>
                      <option value= "7" @if($jadwal->id_ekstrakurikuler == "7") selected @endif>Al-Banjari</option>
                      <option value= "6" @if($jadwal->id_ekstrakurikuler == "6") selected @endif>Bisnis Plan</option>
                      <option value= "5" @if($jadwal->id_ekstrakurikuler == "5") selected @endif>Olahraga Prestasi</option>
                      <option value= "9" @if($jadwal->id_ekstrakurikuler == "9") selected @endif>Komputer</option>
                      <option value= "4" @if($jadwal->id_ekstrakurikuler == "4") selected @endif>Pencak Silat</option>
                      <option value= "8" @if($jadwal->id_ekstrakurikuler == "8") selected @endif>Pendidikan TPQ</option>
                    </select>
                </div>
                  <div class="form-group">
                    <label for="exampleInpuPembina">Pembina</label>
                    <input type="text" class="form-control"  name="pembina" placeholder="Pembina" value="{{$jadwal->pembina}}">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
 @endsection

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<!-- Page specific script -->
</html>
