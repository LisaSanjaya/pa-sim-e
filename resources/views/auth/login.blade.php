<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="{{ url('css/login.css') }}" crossorigin="anonymous">
  
</head>
<body class="c-app flex-row align-items-center" style="background-image: linear-gradient(rgba(16, 104, 34, 0.8), rgba(16, 104, 34, 0.8)),url('{{ asset('images/pens.png') }}');">
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-4">
              <div class="card-group">
                  <div class="card p-4">
                      <div class="card-body">
                          <form method="post" action="/postlogin">
                              @csrf
                              <h1>Login</h1>
                              <p class="text-muted">Sign In to your account</p>
                              <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                      </span>
                                  </div>
                                  <input type="email" class="form-control" name="email" placeholder="Email">
                              </div>
                              <div class="input-group mb-4">
                                  <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <i class="fas fa-lock"></i>
                                      </span>
                                  </div>
                                  <input type="password" class="form-control" placeholder="Password" name="password">
                              </div>
                              <div class="row">
                                  <div class="col-4">
                                      <button class="btn btn-primary px-4" type="submit">Login</button>
                                  </div>
                                  <div class="col-8 text-right">
                                      {{-- <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                                          Forgot password?
                                      </a> --}}
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
</body>
</html>
