@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  @section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pengumuman Ekstrakurikuler</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <a href="/pengumuman/create" class="btn btn-success">Tambah Data </a>
                  <a href="/pengumuman/edit" class="btn btn-primary">Edit Data </a>
                  <br>
                  <thead>
                    <br>
                    <div class="container">
                      <div class="row">
                          <!-- Bagian Pengumuman -->
                          <div class="col-md-12">
                              <div class="timeline">
                                  @foreach ($pengumuman as $pgm)
                                      <div class="time-label">
                                          <span class="bg-red">{{$pgm->waktu}}</span>
                                      </div>
                                      <div>
                                          <i class="fas fa-envelope bg-blue"></i>
                                          <div class="timeline-item">
                                              <h3 class="timeline-header">{{ $ekstrakurikuler->where('id', $pgm->id_ekstrakurikuler)->first()->nama }}</h3>
                                              <div class="timeline-body">
                                                  {{$pgm->pengumuman}}
                                              </div>
                                          </div>
                                      </div>
                                  @endforeach
                              </div>
                  {{-- <tr>
                    <th style="width: 10px">No</th>
                    <th>Tanggal</th>
                    <th>Ekstrakurikuler</th>
                    <th>Pengumuman</th>
                    <th style="width: 150px">Aksi</th>
              </tr>
            </thead>
            @foreach ($pengumuman as $pgm)
        <tr>
          <th style="width: 10px">{{$loop->iteration}}</th>
          <th>{{$pgm->waktu}}</th>
          <td>{{ $ekstrakurikuler->where('id', $pgm->id_ekstrakurikuler)->first()->nama }}</td>
          <th>{{$pgm->pengumuman}}</th>
          <th><a href="/pengumuman/{{$pgm->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit </a>
          <form action="/pengumuman/{{$pgm->id}}" method="post" style="display: inline;">
            @csrf
            @method('delete')
            <button type="submit" value="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>
          </form>
        </th>
      </tr>
            @endforeach --}}
          </table>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
 @endsection

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
