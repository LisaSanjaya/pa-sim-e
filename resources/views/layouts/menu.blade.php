<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="{{url ('home')}}" class="nav-link">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Beranda
        </p>
      </a>
    </li>
    @if (Auth::user()->level ==2 )
    <li class="nav-item">
      <a href="/anggota/create" class="nav-link">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Daftar Ekstrakurikuler
        </p>
      </a>
    </li>
    @endif
    
{{-- anggota --}}
@if (in_array(Auth::user()->level, [1, 2]))
    <li class="nav-item">
      <a href="/anggota_ekstrakurikuler" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 6)
    <li class="nav-item">
      <a href="/anggota_tataboga" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 11)
    <li class="nav-item">
      <a href="/anggota_pramuka" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 12)
    <li class="nav-item">
      <a href="/anggota_tpq" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 13)
    <li class="nav-item">
      <a href="/anggota_pencak" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 9)
    <li class="nav-item">
      <a href="/anggota_olahraga" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 8)
    <li class="nav-item">
      <a href="/anggota_bisnis" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 10)
    <li class="nav-item">
      <a href="/anggota_bisnis" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 7)
    <li class="nav-item">
      <a href="/anggota_banjari" class="nav-link">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
         Anggota 
        </p>
      </a>
    </li>
    @endif

    {{-- jadwal --}}
    @if (Auth::user()->level != 2)
    <li class="nav-item">
      <a href="/jadwal" class="nav-link">
        <i class="nav-icon fas fa-calendar-alt"></i>
        <p>
          Jadwal 
        </p>
      </a>
      @endif
    </li>

    {{-- absensi --}}
    @if (Auth::user()->level == 4)
    <li class="nav-item">
      <a href="/absensi_pembina" class="nav-link">
        <i class="nav-icon fas fa-edit"></i>
        <p>
          Absensi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->level == 2)
    <li class="nav-item">
      <a href="/absensi_siswa" class="nav-link">
        <i class="nav-icon fas fa-edit"></i>
        <p>
          Absensi
        </p>
      </a>
    </li>
    @endif
    {{-- kegiatan --}}
    @if (in_array(Auth::user()->level, [1, 2]))
    <li class="nav-item">
      <a href="/kegiatan" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 6)
    <li class="nav-item">
      <a href="/kegiatan_tataboga" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 7)
    <li class="nav-item">
      <a href="/kegiatan_banjari" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 8)
    <li class="nav-item">
      <a href="/kegiatan_bisnis" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 9)
    <li class="nav-item">
      <a href="/kegiatan_olahraga" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 10)
    <li class="nav-item">
      <a href="/kegiatan_komputer" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 11)
    <li class="nav-item">
      <a href="/kegiatan_pramuka" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 12)
    <li class="nav-item">
      <a href="/kegiatan_tpq" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 13)
    <li class="nav-item">
      <a href="/kegiatan_pencak" class="nav-link">
        <i class="nav-icon fas fa-th-list"></i>
        <p>
          Kegiatan 
        </p>
      </a>
    </li>
    @endif
{{-- nilai --}}
    @if(auth()->user()->level == 1)
    <li class="nav-item">
      <a href="/list_ekstrakurikuler" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 6)
    <li class="nav-item">
      <a href="/nilai_tataboga" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 7)
    <li class="nav-item">
      <a href="/nilai_banjari" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 8)
    <li class="nav-item">
      <a href="/nilai_bisnis" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 9)
    <li class="nav-item">
      <a href="/nilai_olahraga" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 10)
    <li class="nav-item">
      <a href="/nilai_komputer" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 11)
    <li class="nav-item">
      <a href="/nilai_pramuka" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 12)
    <li class="nav-item">
      <a href="/nilai_tpq" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif
    @if(auth()->user()->id == 13)
    <li class="nav-item">
      <a href="/nilai_pencak" class="nav-link">
        <i class="nav-icon fas fa-check"></i>
        <p>
          Nilai
        </p>
      </a>
    </li>
    @endif

    {{-- prestasi --}}
    @if (Auth::user()->level == 2)
    <li class="nav-item">
      <a href="/prestasi" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->level == 1)
    <li class="nav-item">
      <a href="/prestasi_ekstrakurikuler" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 6)
    <li class="nav-item">
      <a href="/prestasi_tataboga" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 7)
    <li class="nav-item">
      <a href="/prestasi_banjari" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 8)
    <li class="nav-item">
      <a href="/prestasi_bisnis" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 9)
    <li class="nav-item">
      <a href="/olahraga" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 10)
    <li class="nav-item">
      <a href="/prestasi_komputer" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 11)
    <li class="nav-item">
      <a href="/prestasi_pramuka" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 12)
    <li class="nav-item">
      <a href="/prestasi_tpq" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->id == 13)
    <li class="nav-item">
      <a href="/prestasi_pencak" class="nav-link">
        <i class="nav-icon fas fa-trophy"></i>
        <p>
          Prestasi
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->level != 2)
    <li class="nav-item">
      <a href="/pengumuman" class="nav-link">
       <i class="nav-icon fas fa-bullhorn"></i>
        <p>
          Pengumuman
        </p>
      </a>
    </li>
    @endif
    @if (Auth::user()->level != 2)
    <li class="nav-item">
      <a href="/laporan" class="nav-link">
        <i class="nav-icon fas fa-print"></i>
        <p>
          Laporan
        </p>
      </a>
    </li>
    @endif
    <br>
    <br>
    <br>
    <li class="nav-item">
      <a href="/logout" class="nav-link">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>
          Log Out
        </p>
      </a>
    </li>
</ul>


