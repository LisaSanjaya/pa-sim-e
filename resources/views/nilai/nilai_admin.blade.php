@extends('layouts.app')

@section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')

@section('content')
    <!-- Main content -->
    <div class="content">
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Nilai Ekstrakurikuler</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Ekstrakurikuler</th>
                                        <th style="width: 225px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td> 1</td>
                                            <td> Tata Boga </td>
                                            <td>
                                                <a href="/nilai_tataboga" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 2</td>
                                            <td> Pramuka </td>
                                            <td>
                                                <a href="/nilai_pramuka" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 3</td>
                                            <td> Pencak Silat </td>
                                            <td>
                                                <a href="/nilai_pencak" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 4 </td>
                                            <td> Olahraga Prestasi </td>
                                            <td>
                                                <a href="/nilai_olahraga" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 5 </td>
                                            <td> Bisnis plan </td>
                                            <td>
                                                <a href="/nilai_bisnis" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 6 </td>
                                            <td> Al-Banjari </td>
                                            <td>
                                                <a href="/nilai_banjari" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 7 </td>
                                            <td> Pendidikan TPQ </td>
                                            <td>
                                                <a href="/nilai_tpq" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 8 </td>
                                            <td> Komputer </td>
                                            <td>
                                                <a href="/nilai_komputer" class="btn btn-primary"> <i class="fas fa-edit"></i> Lihat
                                                </a>
                                            </td>
                                        </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection