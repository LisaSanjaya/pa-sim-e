@extends('layouts.app')

@section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')

@section('content')
        <!-- Main content -->
        <div class="content">
            <br>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Nilai Ekstrakurikuler Olahraga</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">No</th>
                                            <th style="width: 10%">NISN</th>
                                            <th style="width: 20%">Nama Siswa</th>
                                            <th style="width: 10%">Kelas</th>
                                            <th style="width: 20%">Ekstrakurikuler</th>
                                            <th style="width: 10%">Nilai</th>
                                            <th style="width: 20%">Deskripsi</th>
                                            <th style="width: 5%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($nilai->where('id_ekstrakurikuler', '5') as $index => $item)
                                            <tr>
                                                <td>{{$index+1}}</td>
                                                <td>{{$item->nomor_induk}}</td>
                                                <td>{{$item->nama}}</td>
                                                <td>{{$item->kelas}}</td>
                                                <td>{{ $ekstrakurikuler->where('id', $item->id_ekstrakurikuler)->first()->nama }}</td>
                                                <td>{{$item->nilai}}</td>
                                                <td>{{$item->deskripsi}}</td>
                                                <td>
                                                    <a href="/nilai_laporan" class="btn btn-success"><i class="fas fa-edit"></i>Lihat</a>
                                                    {{-- <form action="/nilai" method="post" style="display: inline;"> --}}
                                                        {{-- Form action code here --}}
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

<!-- Scripts -->
@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="../../plugins/jszip/jszip.min.js"></script>
    <script src="../../plugins/pdfmake/pdfmake.min.js"></script>
    <script src="../../plugins/pdfmake/vfs_fonts.js"></script>
    <script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- Page specific script -->
@endsection