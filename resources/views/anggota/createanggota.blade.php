@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
  <!-- jQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <!-- Select2 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

  @section('title', 'ANGGOTA EKSTRAKULIKULER | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

              <div class="card card-success">
                <div class="card-header">
                <h3 class="card-title">Daftar Anggota Ekstrakurikuler</h3>
              </div>
              @if(session('error'))
              <div class="alert alert-danger">
                {{ session('error') }}
              </div>
            @endif
              <!-- /.card-header -->
              <form action="/anggota/store" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputNama">Nama</label>
                    <input type="text" class="form-control"  name="nama" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNama">Email</label>
                    <input type="email" class="form-control"  name="email" placeholder="email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNISN">NISN</label>
                    <input type="text" class="form-control" name="nomor_induk" placeholder="NISN">
                  </div>
                  <div class="form-group">
                    <label>Kelas </label>
                    <select class="form-control" name="kelas">
                      <option>X IPA</option>
                      <option>X IPS</option>
                      <option>XI IPA</option>
                      <option>XI IPS</option>
                      <option>XII IPA</option>
                      <option>XII IPS</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Ekstrakurikuler</label>
                    <select class="form-control" name="ekstrakurikuler_id">
                      @foreach ($ekstrakurikuler as $ekstra)
                      <option value="{{ $ekstra->id }}">{{$ekstra->nama}}</option>
                  @endforeach
                </select>
                </div>
                <div class="form-group">
                    <label>Tahun Pelajaran</label>
                    <select class="form-control" name="tapel">
                        <option>2022/2023</option>
                        <option>2023/2024</option>
                    </select>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" name="submit" class="btn btn-primary">Daftar</button>
                
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- Page specific script -->
</html>

<script>
  $(document).ready(function() {
      $('.select2').select2();
  });
</script>