@extends('layouts.app')

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

@section('title', 'ANGGOTA EKSTRAKULIKULER | SIM EKSKUL')
@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Anggota Ekstrakurikuler Tata Boga</h3>
                        </div>
                        
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <label class="mr-1 mb-0" style="white-space: nowrap;">Tahun Ajaran</label>
                                    <select id="tapel" class="select2" style="width: 100%;" data-order-by="tapel">
                                        <option value="Semua">Tahun Ajaran</option>
                                        <option value="2023/2024">2023/2024</option>
                                        <option value="2022/2023">2022/2023</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>NISN</th>
                                        <th>Nama Siswa</th>
                                        <th>Kelas</th>
                                        <th>Ekstrakurikuler</th>
                                        @if (auth()->user()->level === 1)
                                        <th style="width: 225px">Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($anggota->where('ekstrakurikuler_id', '1') as $index => $agt)
                                        <tr data-tapel="{{$agt->tapel}}">
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$agt->nomor_induk}}</td>
                                            <td>{{$agt->nama}}</td>
                                            <td>{{$agt->kelas}}</td>
                                            <td>{{$agt->ekstrakurikuler->nama}}</td>
                                            <td>
                                                @if (auth()->user()->level === 1)
                                                <a href="/tataboga/{{$agt->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</a>
                                                <form action="/tataboga/{{$agt->id}}" method="post" style="display: inline;">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" value="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>
                                                </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- Page specific script -->
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "order": [[2, "asc"]] // Kolom 2 adalah kolom tahun ajaran, "asc" untuk ascending
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        
        $('#tapel').on('change', function() {
            var selectedTapel = $(this).val();
            if (selectedTapel === 'Semua') {
                // Tampilkan semua data
                $('#example1 tbody tr').show();
            } else {
                // Sembunyikan semua data
                $('#example1 tbody tr').hide();
                // Tampilkan data yang terkait dengan tahun ajaran yang dipilih
                $('#example1 tbody tr[data-tapel="' + selectedTapel + '"]').show();
            }
        });
    });
</script>


</body>
</html>