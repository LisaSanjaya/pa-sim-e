@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  @section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')
  @section('content')

  <br>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              {{ $ekstrakurikuler->nama }}
            </h3>
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <h6>NIP</h6>
            <h6>Nama Pembina</h6>
            <br>
            <form action="{{ route('absen.siswa') }}" method="POST">
              @csrf
        
        @if($result = App\Models\presensi::where('ekstrakurikuler_id', $anggota->ekstrakurikuler_id)
        ->where('status', 'buka')->first())
        <input type="hidden" name="presensi_id" value="{{$result->id}}">
        @if($sudahpresensi = App\Models\history_presensi::where('presensi_id', $result->id)->first())
        <button disabled class="btn btn-block btn-danger" id="presensi-siswa">Sudah Presensi</button>  
        @else 
       <button type="submit" class="btn btn-block btn-primary" id="presensi-siswa">Presensi</button>  
       @endif
       @else <button disabled type="submit" class="btn btn-block btn-primary" id="presensi-siswa">Presensi</button> 
       @endif
    </form>
</div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
                <h3 class="card-title">History Presensi</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th >ID</th>
                    <th>Tanggal</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    @foreach ($absensi as $abs)
                    <td >{{$abs->id}}</td>
                    <td>{{$abs->tanggal}}</td>
                     {{-- <td><form action="{{ route('absen.destroy', $abs->id) }}" method="POST">
                      @csrf
                      @method('delete')
                      <button type="submit" value="delete" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                    </form> --}}
              </td>
              </tr>
              @endforeach 
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @endsection
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "pageLength": 3 ,
    });
  });
</script>
<style>
  .small-column {
    width: 50px;
  }
</style>
</body>
</html>
