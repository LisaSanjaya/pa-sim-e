@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  @section('title', 'PRESTASI ANGGOTA | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Prestasi Ekstrakurikuler Al-Banjari</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <a href="prestasi/create" class="btn btn-success">Tambah Data </a>
                  <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th> NISN</th>
                    <th> Nama </th>
                    <th> Kelas</th>
                    <th> Ekstrakurikuler</th>
                    <th>Prestasi</th>
                    {{-- <th>Sertifikat</th> --}}
                    <th style="width: 150px">Aksi</th>
                  </tr>
                  </thead>
                  @foreach ($prestasi->where('ekstrakurikuler', 'Al-Banjari') as $index => $prs)
              <tr>
                <th style="width: 10px">{{$loop->iteration}}</th>
                <th>{{$prs->nomor_induk}}</th>
                <th>{{$prs->nama}}</th>
                <th>{{$prs->kelas}}</th>
                <td>{{$ekstrakurikuler->where('id', $prs->id_ekstrakurikuler)->first()->nama }}</td>
                <th>{{$prs->prestasi}}</th>
                {{-- <th>
                  <img src="{{asset('storage/foto').'/'.$prs->foto}}" width="200px"> 

                </th> --}}
                <th><a href="/prestasi/{{$prs->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit </a>
                <form action="/prestasi/{{$prs->id}}" method="post" style="display: inline;">
                  @csrf
                  @method('delete')
                  <button type="submit" value="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>
                </form>
                </th>
              </tr>
              @endforeach

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
 @endsection

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
