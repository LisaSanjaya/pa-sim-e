@extends('layouts.app')

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  @section('title', 'ANGGOTA EKSTRAKULIKULER | SIM EKSKUL')
  @section('content')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              </div>

              <div class="card card-success">
                <div class="card-header">
                <h3 class="card-title">Tambah Prestasi Anggota Ekstrakurikuler</h3>
              </div>
              <!-- /.card-header -->
              <form action="/prestasi/store" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputNISN">NISN</label>
                    <input type="text" class="form-control" name="nomor_induk" placeholder="NISN">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNama">Nama</label>
                    <input type="text" class="form-control"  name="nama" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label>Kelas </label>
                    <select class="form-control" name="kelas">
                      <option>X IPA</option>
                      <option>X IPS</option>
                      <option>XI IPA</option>
                      <option>XI IPS</option>
                      <option>XII IPA</option>
                      <option>XII IPS</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Ekstrakurikuler</label>
                    <select class="form-control" name="id_ekstrakurikuler">
                      @foreach ($ekstrakurikuler as $ekstra)
                      <option value="{{ $ekstra->id }}">{{$ekstra->nama}}</option>
                  @endforeach
                </select>
                </div>
                  <div class="form-group">
                    <label for="exampleInputPrestasi">Prestasi</label>
                    <input type="text" class="form-control"  name="prestasi" placeholder="Prestasi">
                  {{-- </div>
                    <div class="form-group">
                    <label for="foto" class="form-label">Sertifikat</label>
                    <input type="file" name="foto" id="foto" class="form-control" accept="image/*">
                  </div> --}}
                  <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
 @endsection

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<!-- Page specific script -->
<!-- bs-custom-file-input -->
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
</html>