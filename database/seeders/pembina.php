<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\facades\DB;
use Illuminate\Support\Facades\Hash;

class pembina extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pembina')->insert([
            'name' => 'Pembina Tata Boga',
            'email' => 'tataboga@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Tata Boga'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Al-Banjari',
            'email' => 'al-banjari@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Al-Banjari'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Busnis Plan',
            'email' => 'Bisnisplan@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Bisnis Plan'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Olahraga Prestasi',
            'email' => 'olahraga@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Olahraga Prestasi'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Komputer',
            'email' => 'komputer@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Komputer'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Pramuka',
            'email' => 'pramuka@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Pramuka'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Pendidikan TPQ',
            'email' => 'pendidikan@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Pendidikan TPQ'
        ]);
        DB::table('pembina')->insert([
            'name' => 'Pembina Pencak Silat',
            'email' => 'pencaksilat@gmail.com',
            'password' => Hash::make('password'),
            'ekstrakurikuler' => 'Pencak Silat'
        ]);
    }
}
