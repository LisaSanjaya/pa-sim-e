<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Ulinnuha Puwonintyas',
                'email' => 'ulin@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Widya Estifani',
                'email' => 'widya@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Nihayatul Khasanah',
                'email' => 'yatul@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Lizza diana manzil',
                'email' => 'lizza@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Hayyina nabilah',
                'email' => 'bela@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Musyarrofatul Lisa',
                'email' => 'lisaa@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Kholifuddin Al-islami',
                'email' => 'kholif@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Halimatus Sa’diya',
                'email' => 'nada@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Khoirur Rozikin',
                'email' => 'celing@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Muhammad sholeh faisho',
                'email' => 'fais@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
            [
                'name' => 'Risalah Tazkiyah',
                'email' => 'risa@gmail.com',
                'password' => bcrypt('password'),
                'level' => 2
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
